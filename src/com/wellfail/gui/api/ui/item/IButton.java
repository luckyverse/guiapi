package com.wellfail.gui.api.ui.item;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * Created by WellFail on 19.01.2016.
 */
public interface IButton {
    void onClick(Player player, ClickType clickType);
}
