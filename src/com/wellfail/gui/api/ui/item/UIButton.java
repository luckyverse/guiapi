package com.wellfail.gui.api.ui.item;


import com.wellfail.gui.api.ui.UserInterface;

/**
 * Created by WellFail on 24.01.2016.
 */
public abstract class UIButton implements IButton{

    private UserInterface ui;

    public UserInterface getUi(){
        return ui;
    }

    public UIButton(UserInterface ui){
        this.ui = ui;
    }

}
