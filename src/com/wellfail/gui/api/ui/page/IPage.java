package com.wellfail.gui.api.ui.page;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by WellFail on 19.01.2016.
 */
public interface IPage {
    void playerClicked(InventoryClickEvent event);
}
