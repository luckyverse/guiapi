package com.wellfail.gui.api.ui.page;

import com.wellfail.gui.api.ui.UserInterface;
import com.wellfail.gui.api.ui.item.IButton;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.wellfail.wapi.Main;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by WellFail on 19.01.2016.
 */

public abstract class UIPage implements IPage {

    private HashMap<Integer, IButton> buttons = new HashMap<>();

    private int pageSize;
    private int pageId;
    private UserInterface ui;
    private Player player;

    private String pageName;

    private Inventory pInventory;

    public Inventory getInventory() {
        return pInventory;
    }

    public String getPageName() {
        return this.pageName;
    }

    public UserInterface getUi() {
        return this.ui;
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getPageId() {
        return this.pageId;
    }

    public UIPage(int pageId, int pageSize, String pageName, UserInterface ui, Player player) {
        this.pageSize = pageSize;
        this.pageId = pageId;
        this.pageName = pageName;
        this.ui = ui;
        this.player = player;
        init();
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void init() {
        pInventory = Bukkit.createInventory(null, pageSize, pageName);
        fillPage();
    }

    public void close(Player player) {
        player.closeInventory();
        close(player, getInventory());
    }

    public void reOpen(Player player){
        fillPage();
    }

    public ItemStack moneyItem(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        String[] lore = new String[]{"§e" + (int) Main.economy.getBalance(this.player.getName()) +"§7 монеток"};
        meta.setDisplayName("§7Баланс");
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    protected abstract void close(Player player, Inventory inv);

    protected abstract void fillPage();

    public abstract void onOpen(Player player);


    public void addButton(int slot, IButton button, ItemStack item) {
        buttons.put(slot, button);
        if (slot < pInventory.getSize() && slot > -1) {
            pInventory.setItem(slot, item);
        }
    }

    public void removeButton(IButton button) {
        buttons.remove(button);
    }

    public void removeButton(int id) {
        removeButton(buttons.get(id));
    }

    @Override
    public void playerClicked(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (buttons.containsKey(event.getRawSlot())) {
            if (buttons.get(event.getRawSlot()) != null) {
                buttons.get(event.getRawSlot()).onClick(player, event.getClick());
                playAcceptSound(player);
            }
        } else if (event.getRawSlot() != -999) {
            if (event.getInventory().getTitle() == getPageName() && (pInventory.getSize() <= event.getSlot() || pInventory.getItem(event.getSlot()) != null)) {
                playDenySound(player);
            } else if (event.getInventory() == player.getInventory() && player.getInventory().getItem(event.getSlot()) != null) {
                playDenySound(player);
            }
        }
    }

    public void playAcceptSound(Player player) {
        player.playSound(player.getLocation(), Sound.CLICK, 1, 1.6f);
    }

    public void playRemoveSound(Player player) {
        player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 0.6f);
    }

    public void playDenySound(Player player) {
        player.playSound(player.getLocation(), Sound.ITEM_BREAK, 1, .6f);
    }
}
