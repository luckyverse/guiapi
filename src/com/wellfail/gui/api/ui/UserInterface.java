package com.wellfail.gui.api.ui;

import com.wellfail.gui.api.ui.page.UIPage;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.event.CraftEventFactory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

/**
 * Created by WellFail on 19.01.2016.
 */
public class UserInterface implements Listener {

    private Plugin plugin;


    private HashMap<String, UIPage> playerPages = new HashMap<>();

    private UIPage mainPage;

    public UserInterface(Plugin plugin) {
        this.plugin = plugin;
        this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    public void open(Player player) {
        if(mainPage != null){
            openPageForPlayer(player,mainPage);
        }else{
            player.sendMessage("Ошибка в гуи. Сообщите администрации.");
        }
    }

    public void setMainPage(UIPage page){
        this.mainPage = page;
    }

    public void close(Player player) {
        player.closeInventory();
        playerPages.remove(player.getName());
    }

    public void setPageForPlayer(Player player,UIPage page){
        String name = player.getName();
        if(playerPages.containsKey(name)){
            playerPages.get(name).close(player);
        }
        playerPages.put(name,page);
    }

    public void openPageForPlayer(Player player, UIPage page){
        page.setPlayer(player);
       page.onOpen(player);

        setPageForPlayer(player,page);

        EntityPlayer nmsPlayer = ((CraftPlayer) player).getHandle();
        if (nmsPlayer.activeContainer != nmsPlayer.defaultContainer)
        {
            CraftEventFactory.handleInventoryCloseEvent(nmsPlayer);
            nmsPlayer.m();
        }
        player.openInventory(page.getInventory());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (playerPages.containsKey(event.getWhoClicked().getName()) && playerPages.get(event.getWhoClicked().getName()).getPageName().equalsIgnoreCase(event.getInventory().getName())) {
            playerPages.get(event.getWhoClicked().getName()).playerClicked(event);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        playerPages.remove(event.getPlayer().getName());
    }


}
